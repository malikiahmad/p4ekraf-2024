<?php

namespace Database\Seeders;

use App\Models\Auth\Permission;
use App\Models\Auth\Role;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    public function run()
    {
        $permissions = [

            /** DASHBOARD **/
            [
                'name'          => 'dashboard',
                'action'        => ['view'],
            ],

            /** ADMIN CONSOLE **/
            [
                'name'          => 'master',
                'action'        => ['view'],
            ],
            [
                'name'          => 'master_training_type',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'          => 'master_training',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'          => 'master_certificate',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],

            // Setting
            [
                'name'          => 'setting',
                'action'        => ['view'],
            ],
            [
                'name'          => 'setting_role',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'          => 'setting_permission',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'          => 'setting_user',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'          => 'setting_activity',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],

            // TRansaction
            [
                'name'          => 'participant',
                'action'        => ['view'],
            ],
            [
                'name'          => 'participant_training',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],
            // participant_certificate
            [
                'name'          => 'participant_certificate',
                'action'        => ['view', 'create', 'edit', 'delete'],
            ],
        ];

        $this->command->getOutput()->progressStart($this->countActions($permissions));
        $this->generate($permissions);
        $this->command->getOutput()->progressFinish();
    }

    public function generate($permissions)
    {
        // Role
        $admin = Role::firstOrCreate(
            ['id' => 1],
            [
                'name' => 'Administrator',
            ]
        );

        $perms_ids = [];
        foreach ($permissions as $row) {
            foreach ($row['action'] as $key => $val) {
                $this->command->getOutput()->progressAdvance();

                $name = $row['name'] . '.' . trim($val);
                $perms = Permission::firstOrCreate(compact('name'));
                $perms_ids[] = $perms->id;

                if (!$admin->hasPermissionTo($perms->name)) {
                    $admin->givePermissionTo($perms);
                }
            }
        }
        Permission::whereNotIn('id', $perms_ids)->delete();


        // Clear Perms Cache
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
    }

    public function countActions($data)
    {
        $count = 0;
        foreach ($data as $row) {
            $count += count($row['action']);
        }

        return $count;
    }
}
