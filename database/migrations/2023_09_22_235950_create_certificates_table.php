<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("training_type_id");
            $table->foreign("training_type_id")->references("id")->on("training_types")->onUpdate("cascade")->onDelete("cascade");
            $table->string("code", 30)->unique();
            $table->string("name")->nullable($value = false)->index();
            $table->string("slug", 150)->nullable($value = false)->unique()->index();
            $table->string("description", 255)->nullable($value = false);
            $table->string("place");
            $table->string("address");
            $table->string("link_wa_group");
            $table->string("syarat_ketentuan");
            $table->date("start_date_registration")->nullable($value = false);
            $table->date("end_date_registration")->nullable($value = false);
            $table->date("start_date")->nullable($value = false);
            $table->date("end_date")->nullable($value = false);
            $table->unsignedInteger("duration");
            $table->unsignedBigInteger("quota")->nullable($value = false);
            $table->string("logo")->nullable();
            $table->string("image")->nullable();
            $table->commonFields();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
