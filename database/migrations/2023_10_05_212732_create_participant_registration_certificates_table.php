<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantRegistrationCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_registration_certificates', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger("user_id");
            $table->foreign("user_id")->references("id")->on("sys_users")->onUpdate("cascade");

            $table->unsignedBigInteger("certificate_id");
            $table->foreign("certificate_id")->references("id")->on("certificates")->onUpdate("cascade");

            $table->string("participant_code", 35)->nullable(false)->unique()->index();
            $table->date("registration_date")->nullable($value = false)->index();
            $table->enum("status", ["registered", "dropout"])->default("registered");
            $table->string("nik", 16)->nullable(false)->index();
            $table->enum("type_of_ktp", ["dki", 'non-dki'])->nullable(false)->default("dki")->index();
            $table->string("birth_place")->nullable(false);
            $table->date("birth_date")->nullable(false);
            $table->enum("gender", ["p", "l"])->nullable(false);
            $table->string("residence_address", 255)->nullable(true);
            $table->string("identity_address", 255)->nullable(false);
            $table->string("phone_no", 16)->nullable(false)->index();
            $table->string("work_experience", 255)->nullable(true);
            $table->string("current_job")->nullable(true);
            $table->string("name_of_business_or_work_place", 75)->nullable(true);
            $table->string("last_education", 75)->nullable();
            // file ktp
            $table->string("id_card_file")->nullable(true);
            // file ijazah terakhir
            $table->string("last_diploma_file")->nullable(true);
            $table->string("cert_of_employment_file")->nullable(true);
            $table->string("cv_file")->nullable(true);
            $table->string("other_cert_file")->nullable(true);

            $table->string("pakta_integritas")->nullable(true);
            $table->boolean('isPretest')->default(false);
            $table->commonFields();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_registration_certificates');
    }
}
