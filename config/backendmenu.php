<?php

return [

    [
        'section' => 'NAVIGASI',
        'name' => 'navigasi',
        'perms' => 'dashboard',
    ],
    // Dashboard
    [
        'name'      => 'dashboard',
        'perms'     => 'dashboard',
        'title'     => 'Dashboard',
        'icon'      => 'fa fa-th-large',
        'url'       => '/admin/home',
    ],

    // General
    [
        'section' => 'GENERAL',
        'name' => 'general',
    ],
    // Transaction
    [
        'name' => 'participant',
        'perms' => 'participant',
        'title' => 'Peserta',
        'icon' => 'fa fa-address-card',
        "submenu" => [
            [
                'name' => 'transaction_participant',
                'perms' => 'participant_training',
                'title' => 'Pelatihan',
                'url' => '/admin/transaction/participants',
            ],
            [
                'name' => 'transaction_participant_certificate',
                'perms' => 'participant_certificate',
                'title' => 'Sertifikasi',
                'url' => '/admin/transaction/certificates',
            ],
        ],
    ],

    // Admin Konsol
    [
        'section' => 'ADMIN KONSOL',
        'name' => 'console_admin',
    ],
    // Master
    [
        'name' => 'master',
        'perms' => 'master',
        'title' => 'Master',
        'icon' => 'fa fa-folder-open',
        "submenu" => [
            [
                'name' => 'master_training_type',
                'title' => 'Tipe',
                'url' => '/admin/master/training-types',
                "perms" => "master_training_type",
            ],
            [
                'name' => 'master_training',
                'title' => 'Pelatihan',
                'url' => '/admin/master/trainings',
                "perms" => "master_training",
            ],
            [
                'name' => 'master_certificate',
                'title' => 'Sertifikasi',
                'url' => '/admin/master/certificates',
                "perms" => "master_certificate",
            ],
        ],
    ],

    // Setting
    [
        'name' => 'setting',
        'perms' => 'setting',
        'title' => 'Pengaturan Umum',
        'icon' => 'fa fa-cogs',
        'submenu' => [
            [
                'name' => 'setting_role',
                'title' => 'Hak Akses',
                'url' => '/admin/setting/role',
                'perms' => 'setting_role',
            ],
            [
                'name' => 'setting_permission',
                'title' => 'Izin Akses',
                'url' => '/admin/setting/permission',
                'perms' => 'setting_permission',
            ],
            // [
            //     'name' => 'setting_flow',
            //     'title' => 'Flow Approval',
            //     'url' => '/admin/setting/flow',
            // ],
            [
                'name' => 'setting_user',
                'title' => 'Manajemen User',
                'url' => '/admin/setting/user',
                'perms' => 'setting_user',
            ],
            [
                'name' => 'setting_activity',
                'title' => 'P4EKRAF',
                'url' => '/admin/setting/activity',
                'perms' => 'setting_activity',
            ],
        ]
    ],
];
