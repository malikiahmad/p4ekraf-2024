<?php

return [
    'app' => [
        'name' => env('APP_NAME', 'Laravel'),
        'version' => 'v1.0.0',
        'copyright' => '2023 All Rights Reserved',
    ],

    'company' => [
        'key' => 'p4ekraf',
        'name' => 'Dinas Pariwisata Dan Ekonomi Kreatif',
        'phone' => '(021) 3104506',
        'address' => "Jl. Kuningan Barat Raya No.2, RT.1/RW.1, Kuningan Barat, Kec. Mampang Prapatan., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12710 Indonesia.",
        'city' => 'Jakarta'
    ],

    'logo' => [
        'favicon' => 'assets/media/logos/logo-jakarta-raya.ico',
        'auth' => 'assets/media/logos/logo-jakarta-raya-auth.png',
        'aside' => 'assets/media/logos/logo-jakarta-raya-aside.png',
        'print' => 'assets/media/logos/logo-jakarta-raya-print.jpg',
        'barcode' => 'assets/media/logos/logo-jakarta-raya-barcode.jpg',
    ],

    'mail' => [
        'send' => env('MAIL_SEND_STATUS', false),
        'logo' => 'https://4.bp.blogspot.com/-mT0Pz4vwQgQ/Wo43TcRqGUI/AAAAAAAAOeo/fRExSxqAXOE8e6zb7Ft2xKvwch9N1UeaQCLcBGAs/s1600/LOGO-BANK-BPD-DIY-transparent.png',
    ],

    'custom-menu' => true,
];
