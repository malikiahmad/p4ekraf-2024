/**
 *  This Function for remove file, on module
 *  with params
 *  id integer -> id file on database
 *  name string -> name file on database
 *  url string -> for action delete file
 *
 */
 const removeFileModule = (id, name, url) => {
    Swal.fire({
        title: 'Are you sure?',
        text: "You will delete " + name,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: `${url}`,
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "_method" : "DELETE",
                    "_token" : $('meta[name="csrf-token"]').attr('content'),
                    "id" : id,
                },
                success: function (response) {
                    container.remove();
                },
                error: function (response) {
                    $.gritter.add({
                        title: 'Failed!',
                        text:  response.message ?? "Terjadi kesalahan. Data gagal dihapus.",
                        image: BaseUtil.getUrl('assets/media/ui/cross.png'),
                        sticky: false,
                        time: '3000'
                    });
                }
            });
        }
    });
};

$(".base-form--remove-files").on("click", function(e) {
    e.preventDefault();
    let elm = $(this),
        container = elm.closest('.progress-container'),
        parent = container.parent();

    let id = elm.attr("data-id") ? elm.attr('data-id') : null,
        name = elm.attr("data-name") ? elm.attr("data-name") : null;

    if(id != null) removeFileModule(id, name, `/admin/ajax/file/${id}`);

});
