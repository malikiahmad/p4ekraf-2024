<?php

use Illuminate\Support\Facades\Route;

Route::redirect('/', '/admin/home');

Route::prefix("admin")->group(function(){
    Auth::routes();
});

Route::middleware('auth')
    ->prefix("admin")
    ->group(
        function () {
            Route::namespace('Dashboard')
            ->group(
                function () {
                    Route::get('home', 'DashboardController@index')->name('home');
                    Route::post('progress', 'DashboardController@progress')->name('dashboard.progress');
                    Route::post('chartFinding', 'DashboardController@chartFinding')->name('dashboard.chartFinding');
                    Route::post('chartFollowup', 'DashboardController@chartFollowup')->name('dashboard.chartFollowup');
                    Route::post('chartStage', 'DashboardController@chartStage')->name('dashboard.chartStage');
                    Route::get('language/{lang}/setLang', 'DashboardController@setLang')->name('setLang');
                }
            );

            // Ajax
            Route::prefix('ajax')
                ->name('ajax.')
                ->group(
                    function () {
                        Route::post('saveTempFiles', 'AjaxController@saveTempFiles')->name('saveTempFiles');
                        Route::delete('file/{id}', 'AjaxController@deletTempFile')->name('deleteTempFile');
                        Route::get('testNotification/{emails}', 'AjaxController@testNotification')->name('testNotification');
                        Route::post('userNotification', 'AjaxController@userNotification')->name('userNotification');
                        Route::get('userNotification/{notification}/read', 'AjaxController@userNotificationRead')->name('userNotificationRead');
                        // Ajax Modules
                        Route::post('{search}/selectRole', 'AjaxController@selectRole')->name('selectRole');
                        Route::post('{search}/selectUser', 'AjaxController@selectUser')->name('selectUser');
                        Route::post('{search}/selectTrainingType', 'AjaxController@selectTrainingType')->name('selectTrainingType');
                        Route::post('{search}/selectTraining', 'AjaxController@selectTraining')->name('selectTraining');
                    }
                );

            // Setting
            Route::namespace('Setting')
            ->prefix('setting')
            ->name('setting.')
            ->group(
                function () {
                    Route::namespace('Role')
                        ->group(
                            function () {
                                Route::get('role/import', 'RoleController@import')->name('role.import');
                                Route::post('role/importSave', 'RoleController@importSave')->name('role.importSave');
                                Route::get('role/{record}/permit', 'RoleController@permit')->name('role.permit');
                                Route::patch('role/{record}/grant', 'RoleController@grant')->name('role.grant');
                                Route::grid('role', 'RoleController');
                            }
                        );
                    Route::namespace('Permission')
                    ->group(
                        function () {
                            Route::get('permission/import', 'PermissionController@import')->name('permission.import');
                            Route::post('permission/importSave', 'PermissionController@importSave')->name('permission.importSave');
                            Route::grid('permission', 'PermissionController');
                        }
                    );
                    // Route::namespace('Flow')
                    //     ->group(
                    //         function () {
                    //             Route::get('flow/import', 'FlowController@import')->name('flow.import');
                    //             Route::post('flow/importSave', 'FlowController@importSave')->name('flow.importSave');
                    //             Route::grid('flow', 'FlowController');
                    //         }
                    //     );
                    Route::namespace('User')
                        ->group(
                            function () {
                                Route::get('user/import', 'UserController@import')->name('user.import');
                                Route::post('user/importSave', 'UserController@importSave')->name('user.importSave');
                                Route::post('user/{record}/resetPassword', 'UserController@resetPassword')->name('user.resetPassword');
                                Route::grid('user', 'UserController');

                                Route::get('profile', 'ProfileController@index')->name('profile.index');
                                Route::post('profile', 'ProfileController@updateProfile')->name('profile.updateProfile');
                                Route::get('profile/notification', 'ProfileController@notification')->name('profile.notification');
                                Route::post('profile/gridNotification', 'ProfileController@gridNotification')->name('profile.gridNotification');
                                Route::get('profile/activity', 'ProfileController@activity')->name('profile.activity');
                                Route::post('profile/gridActivity', 'ProfileController@gridActivity')->name('profile.gridActivity');
                                Route::get('profile/changePassword', 'ProfileController@changePassword')->name('profile.changePassword');
                                Route::post('profile/changePassword', 'ProfileController@updatePassword')->name('profile.updatePassword');
                            }
                        );
                    Route::namespace('Activity')
                        ->group(
                            function () {
                                Route::get('activity/export', 'ActivityController@export')->name('activity.export');
                                Route::grid('activity', 'ActivityController');
                            }
                        );
                }
            );

            // Master
            Route::namespace("Master")->prefix("master")->name("master.")->group(function() {
                Route::grid("training-types", "TrainingTypeController");
                Route::grid("trainings", "TrainingController");
                Route::grid("certificates", "CertificateController");
            });

            Route::namespace("Transaction")->prefix("transaction")->name("transaction.")->group(function() {
                Route::grid("participants", "Participant\ParticipantController");
                Route::grid("certificates", "Participant\ParticipantCertificateController");
            });

            // Web Transaction Modules
            foreach (\File::allFiles(__DIR__ . '/webs') as $file) {
                require $file->getPathname();
            }
        }
    );
