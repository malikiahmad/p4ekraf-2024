<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace("API")->group(function() {
    Route::post("registration", "AuthController@registration")->name("api.registration");
    Route::post("login", "AuthController@login")->name("api.login");

    /* Pelatihan */
    Route::prefix("training")->group(function() {
        Route::get("/", "TrainingController@index")->name("api.training.index");
        Route::get("/{id}", "TrainingController@show")->name("api.training.show");

        /* Registrasi/Pendaftaran Pelatihan */
        Route::middleware("jwtAuth")->post("/registration", "TrainingController@registration")->name("api.training.registration");
    });

    Route::prefix("certificate")->group(function() {
        Route::get("/", "CertificateController@index")->name("api.certificate.index");
        Route::get("/{id}", "CertificateController@show")->name("api.certificate.show");

        /* Registrasi/Pendaftaran Pelatihan */
        Route::middleware("jwtAuth")->post("/registration", "CertificateController@registration")->name("api.certificate.registration");
    });

    Route::prefix("participant")->group(function() {
        Route::get("/{slug}", "ParticipantController@getListParticipantByTrainingSlug")->name("api.participant.list_by_training_slug");
        Route::get("/detail/{participantCode}", "ParticipantController@detailParticipantByParticipantCode")->name("api.participant.detail_participant_by_participant_code");
    });

    Route::prefix("participant-certificate")->group(function() {
        Route::get("/{slug}", "ParticipantCertificateController@getListParticipantByCertificateSlug")->name("api.participant.list_by_training_slug");
        Route::get("/detail/{participantCode}", "ParticipantCertificateController@detailParticipantByParticipantCode")->name("api.participant.detail_participant_by_participant_code");
    });
});
