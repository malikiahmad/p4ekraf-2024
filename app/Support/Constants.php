<?php
namespace App\Support;

class Constants {
    const PATH_FILE_PARTICIPANT_REGISTRATION = "participant_registration";
    const PATH_FILE_PARTICIPANT_REGISTRATION_CERTIFICATE = "participant_registration_certificate";
}
