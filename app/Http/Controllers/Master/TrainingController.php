<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Master\TrainingRequest;
use App\Models\Master\Training;

class TrainingController extends Controller
{
    protected $module = 'master_training';
    protected $routes = 'master.trainings';
    protected $views = 'master.training';
    protected $perms = 'master_training';

    public function __construct()
    {
        $this->prepare([
            'module' => $this->module,
            'routes' => $this->routes,
            'views' => $this->views,
            'perms' => $this->perms,
            'permission' => $this->perms.'.view',
            'title' => 'Pelatihan',
            'breadcrumb' => [
                'Master' => route($this->routes.'.index'),
                'Pelatihan' => route($this->routes.'.index'),
            ]
        ]);
    }

    public function index()
    {
        $baseContentReplace = "base-content--replace";
        $this->prepare([
            'tableStruct' => [
                'datatable_1' => [
                    $this->makeColumn('name:num'),
                    $this->makeColumn('name:type|label:Tipe|className:text-left'),
                    $this->makeColumn('name:code|label:Kode|className:text-left'),
                    $this->makeColumn('name:name|label:Nama|className:text-left'),
                    $this->makeColumn('name:address|label:Alamat|className:text-left'),
                    $this->makeColumn('name:start_date_registration|label:Tanggal Mulai Pendaftaran|className:text-left'),
                    $this->makeColumn('name:end_date_registration|label:Tanggal Berakhir Pendaftaran|className:text-left'),
                    $this->makeColumn('name:start_date|label:Tanggal Mulai|className:text-left'),
                    $this->makeColumn('name:end_date|label:Tanggal Berakhir|className:text-left'),
                    $this->makeColumn('name:duration|label:Durasi|className:text-left'),
                    $this->makeColumn('name:quota|label:Kuota|className:text-left'),
                    $this->makeColumn('name:updated_by'),
                    $this->makeColumn('name:action'),
                ],
            ],
        ]);
        return $this->render($this->views.'.index', compact("baseContentReplace"));
    }

    public function grid()
    {
        $records = Training::grid()->filters()->dtGet();

        return \DataTables::of($records)
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('type', function ($record) {
                return $record->type->name;
            })
            ->editColumn('duration', function ($record) {
                return $record->duration . " hari";
            })
            ->editColumn('updated_by', function ($record) {
                return $record->createdByRaw();
            })
            ->addColumn('action', function ($record) {
                $actions = [];
                $actions[] = [
                    'type' => 'show',
                    'id' => $record->id,
                    "page" => true,
                ];
                $actions[] = [
                    'type' => 'edit',
                    'id' => $record->id,
                    "page" => true,
                ];

                if ($record->canDeleted()) {
                    $actions[] = [
                        'type' => 'delete',
                        'id' => $record->id,
                        'attrs' => 'data-confirm-text="'.__('Hapus').' Pelatihan '.$record->name.'?"',
                    ];
                }
                return $this->makeButtonDropdown($actions);
            })
            ->rawColumns(['action','updated_by', "duration", "type"])
            ->make(true);
    }

    public function create()
    {
        return $this->render($this->views.'.create');
    }

    public function store(TrainingRequest $request)
    {
        $record = new Training;
        return $record->handleStoreOrUpdate($request);
    }

    public function show(Training $record)
    {
        $page_action = "show";
        return $this->render($this->views.'.show', compact('record', "page_action"));
    }

    public function edit(Training $record)
    {
        $page_action = "edit";
        return $this->render($this->views.'.edit', compact('record', "page_action"));
    }

    public function update(TrainingRequest $request, Training $record)
    {
        return $record->handleStoreOrUpdate($request);
    }

    public function destroy(Certificate $record)
    {
        return $record->handleDestroy();
    }
}
