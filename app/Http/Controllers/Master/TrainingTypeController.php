<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Master\TrainingType as TrainingTypeRequest;
use App\Models\Master\TrainingType;

class TrainingTypeController extends Controller
{
    protected $module = 'master_training_types';
    protected $routes = 'master.training-types';
    protected $views = 'master.training-types';
    protected $perms = 'master_training_type';

    public function __construct()
    {
        $this->prepare([
            'module' => $this->module,
            'routes' => $this->routes,
            'views' => $this->views,
            'perms' => $this->perms,
            'permission' => $this->perms.'.view',
            'title' => 'Tipe Pelatihan',
            'breadcrumb' => [
                'Master' => route($this->routes.'.index'),
                'Tipe Pelatihan' => route($this->routes.'.index'),
            ]
        ]);
    }

    public function index()
    {
        $this->prepare([
            'tableStruct' => [
                'datatable_1' => [
                    $this->makeColumn('name:num'),
                    $this->makeColumn('name:code|label:Kode|className:text-left'),
                    $this->makeColumn('name:name|label:Nama|className:text-left'),
                    $this->makeColumn('name:updated_by'),
                    $this->makeColumn('name:action'),
                ],
            ],
        ]);
        return $this->render($this->views.'.index');
    }

    public function grid()
    {
        $records = TrainingType::grid()->filters()->dtGet();

        return \DataTables::of($records)
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('updated_by', function ($record) {
                return $record->createdByRaw();
            })
            ->addColumn('action', function ($record) {
                $actions = [];
                $actions[] = [
                    'type' => 'show',
                    'id' => $record->id
                ];
                $actions[] = [
                    'type' => 'edit',
                    'id' => $record->id,
                ];

                if ($record->canDeleted()) {
                    $actions[] = [
                        'type' => 'delete',
                        'id' => $record->id,
                        'attrs' => 'data-confirm-text="'.__('Hapus').' Tipe Pelatihan '.$record->name.'?"',
                    ];
                }
                return $this->makeButtonDropdown($actions);
            })
            ->rawColumns(['action','updated_by','status','position'])
            ->make(true);
    }

    public function create()
    {
        return $this->render($this->views.'.create');
    }

    public function store(TrainingTypeRequest $request)
    {
        $record = new TrainingType;
        return $record->handleStoreOrUpdate($request);
    }

    public function show(TrainingType $record)
    {
        return $this->render($this->views.'.show', compact('record'));
    }

    public function edit(TrainingType $record)
    {
        return $this->render($this->views.'.edit', compact('record'));
    }

    public function update(TrainingTypeRequest $request, TrainingType $record)
    {
        return $record->handleStoreOrUpdate($request);
    }

    public function destroy(TrainingType $record)
    {
        return $record->handleDestroy();
    }
}
