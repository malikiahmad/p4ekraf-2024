<?php

namespace App\Http\Controllers\Transaction\Participant;

use App\Http\Controllers\Controller;
use App\Models\Transaction\ParticipantRegistrationCertificate;
use Illuminate\Http\Request;

class ParticipantCertificateController extends Controller
{
    protected $module = 'transaction_participant_certificate';
    protected $routes = 'transaction.certificates';
    protected $views = 'transaction.participant_certificate';
    protected $perms = 'participant_certificate';

    public function __construct()
    {
        $this->prepare([
            'module' => $this->module,
            'routes' => $this->routes,
            'views' => $this->views,
            'perms' => $this->perms,
            'permission' => $this->perms.'.view',
            'title' => 'Peserta Sertifikasi',
            'breadcrumb' => [
                'Transaksi' => route($this->routes.'.index'),
                'Peserta Sertifikasi' => route($this->routes.'.index'),
            ]
        ]);
    }

    public function index()
    {
        $baseContentReplace = "base-content--replace";
        $this->prepare([
            'tableStruct' => [
                'datatable_1' => [
                    $this->makeColumn('name:num'),
                    $this->makeColumn('name:certificate_name|label:Sertifikasi|className:text-left'),
                    $this->makeColumn('name:participant_code|label:Id Peserta|className:text-left'),
                    $this->makeColumn('name:user_fullname|label:Nama Peserta|className:text-left'),
                    $this->makeColumn('name:type_of_ktp|label:Jenis KTP|className:text-left'),
                    $this->makeColumn('name:nik|label:NIK|className:text-left'),
                    $this->makeColumn('name:phone_no|label:Nomor Telepon|className:text-left'),
                    $this->makeColumn('name:registration_date|label:Tanggal Pendaftaran|className:text-left'),
                    $this->makeColumn('name:status|label:Status|className:text-left'),
                    $this->makeColumn('name:updated_by'),
                    $this->makeColumn('name:action'),
                ],
            ],
        ]);
        return $this->render($this->views.'.index', compact("baseContentReplace"));
    }

    public function grid()
    {
        $records = ParticipantRegistrationCertificate::grid()->filters()->dtGet();

        return \DataTables::of($records)
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('user_fullname', function ($record) {
                return $record->user->name;
            })
            ->editColumn('certificate_name', function ($record) {
                return $record->certificate->name;
            })
            ->editColumn('status', function ($record) {
                return strtoupper($record->status);
            })
            ->editColumn('type_of_ktp', function ($record) {
                return strtoupper($record->type_of_ktp);
            })
            ->editColumn('updated_by', function ($record) {
                return $record->createdByRaw();
            })
            ->addColumn('action', function ($record) {
                $actions = [];
                $actions[] = [
                    'type' => 'show',
                    'id' => $record->id,
                    "page" => true,
                ];
                return $this->makeButtonDropdown($actions);
            })
            ->rawColumns(['action','updated_by', "user_fullname", "certificate_name", "status"])
            ->make(true);
    }

    public function show(ParticipantRegistrationCertificate $record)
    {
        $page_action = "show";
        return $this->render($this->views.'.show', compact('record', "page_action"));
    }
}
