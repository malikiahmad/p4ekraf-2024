<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $module =  'dashboard';
    protected $routes =  'dashboard';
    protected $views =  'dashboard';

    public function __construct()
    {
        $this->prepare(
            [
                'module' => $this->module,
                'routes' => $this->routes,
                'views' => $this->views,
                'title' => 'Dashboard',
            ]
        );
    }

    public function index()
    {
        $user = auth()->user();
        if ($user->status != 'active') {
            return $this->render($this->views . '.nonactive');
        }
        if (!$user->checkPerms('dashboard.view') || !$user->roles()->exists()) {
            return abort(403);
        }
        return $this->render($this->views . '.index');
    }

    public function setLang($lang)
    {
        if (\Cache::has('userLocale')) {
            \Cache::forget('userLocale');
        }
        \Cache::forever('userLocale', $lang);
        return redirect()->back();
    }
}
