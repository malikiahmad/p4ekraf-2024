<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DetailParticipantCertificateByParticipantCodeResoucre;
use App\Http\Resources\ListParticipantCertificateBySlugResource;
use App\Models\Transaction\ParticipantRegistrationCertificate;
use Illuminate\Http\Request;

class ParticipantCertificateController extends BaseController
{
    public function getListParticipantByCertificateSlug($slug, Request $request, ParticipantRegistrationCertificate $model) {
        $limit = $request->limit ?? 10;
        $list = $model->with("certificate", "user")
        ->whereHas("certificate", function($query) use($slug) {
            $query->where("slug", $slug);
        })->paginate($limit);

        $response = (new ListParticipantCertificateBySlugResource($list))->toArray($request);

        return $this->sendResponseSuccess($response);
    }

    public function detailParticipantByParticipantCode($participantCode, ParticipantRegistrationCertificate $model) {
        $data = $model->with("certificate", "user")->where("participant_code", $participantCode)->first();

        if(!$data) return $this->sendResponseError([], "Resource Not Found", 404);

        $response = (new DetailParticipantCertificateByParticipantCodeResoucre($data))->toArray(request());

        return $this->sendResponseSuccess($response);
    }
}
