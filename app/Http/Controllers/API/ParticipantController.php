<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\DetailParticipantByParticipantCodeResoucre;
use App\Http\Resources\ListParticipantBySlugResource;
use App\Models\Transaction\ParticipantRegistration;
use Illuminate\Http\Request;

class ParticipantController extends BaseController
{
    public function getListParticipantByTrainingSlug($slug, Request $request, ParticipantRegistration $model) {
        $limit = $request->limit ?? 10;
        $list = $model->with("training", "user")
        ->whereHas("training", function($query) use($slug) {
            $query->where("slug", $slug);
        })->paginate($limit);

        $response = (new ListParticipantBySlugResource($list))->toArray($request);

        return $this->sendResponseSuccess($response);
    }

    public function detailParticipantByParticipantCode($participantCode, ParticipantRegistration $model) {
        $data = $model->with("training", "user")->where("participant_code", $participantCode)->first();

        if(!$data) return $this->sendResponseError([], "Resource Not Found", 404);

        $response = (new DetailParticipantByParticipantCodeResoucre($data))->toArray(request());

        return $this->sendResponseSuccess($response);
    }
}
