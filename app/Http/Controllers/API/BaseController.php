<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponseSuccess($data, $message = '')
    {
        $additionalData = [
            'success' => true,
            'message' => $message,
        ];

        if (is_array($data) === true) {
            // pure array not collection

            if(array_key_exists("data", $data)) {
                $response = array_merge($additionalData, $data);
            } else {
                $response = array_merge($additionalData, ['data' => $data]);
            }
        }else{
            // Collection
            $response = $data->additional($additionalData);
        }

        return $response;
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponseError($data = [], $errorMessages='', $code = 400)
    {
        $response = [
            'success' => false,
            'message' => $errorMessages,
        ];
        if (! empty($data)) {
            $response['data'] = $data;
        }

        return response()->json($response, $code);
    }

    public function sendResponseValidationError(array $data, string $meessage = "Validation errors", int $code = 422) {
        $response = [
            'success' => false,
            'message' => $meessage,
        ];

        $tempData = [];

        if(!empty($data)) {
            foreach($data as $key => $row) {
                $temp = [
                    "field" => $key,
                    "message" => $row[0]
                ];
                array_push($tempData, $temp);
            }
        }
        $response["data"] = $tempData;
        return response()->json($response, $code);
    }
}
