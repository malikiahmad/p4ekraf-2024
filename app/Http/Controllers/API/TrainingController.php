<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\TrainingResource;
use App\Http\Resources\TrainingResourceShow;
use App\Models\Master\Training;
use Illuminate\Http\Request;
use App\Http\Requests\API\ParticipantRegistrationRequest;
use App\Models\Transaction\ParticipantRegistration;

class TrainingController extends BaseController
{
    public function index(Request $request, Training $trainingModel) {
        $data = $trainingModel->with("gambar", "gambar_logo", "type")->grid()->filters()->dtGet()->paginate($request->get("limit") ?? 10);
        
        $response = (new TrainingResource($data))->toArray($request);

        return $this->sendResponseSuccess($response);
    }

    public function show($slug, Training $trainingModel) {
        $training = $trainingModel->whereSlug($slug)->first();

        if(!$training) { return $this->sendResponseError([], "Resource Not Found", 404); }

        $response = (new TrainingResourceShow($training))->toArray(request());

        return $this->sendResponseSuccess($response);
    }

    public function registration(ParticipantRegistrationRequest $request) {
        $input = $request->validated();
        $model = new ParticipantRegistration;

        $input["user_id"] = auth("api")->user()->id;
        $input["participant_code"] = time();
        $input["registration_date"] = date("Y-m-d", time());
        $input["id_card_file"] = $model->handleUploadIdCardFile($request);
        $input["last_diploma_file"] = $model->handleUploadLastDiplomaFile($request);
        $input["cert_of_employment_file"] = $model->handleUploadCertOfEmplymentFile($request);
        $input["cv_file"] = $model->handleUploadCVFile($request);
        $input["other_cert_file"] = $model->handleUploadOtherCertFile($request);

        return $model->handleStoreOrUpdate($input);
    }
}
