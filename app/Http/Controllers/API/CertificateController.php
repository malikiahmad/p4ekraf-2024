<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\ParticipantRegistrationCertificateRequest;
use App\Http\Resources\DetailCertificateResource;
use App\Http\Resources\ListCertificateResource;
use App\Models\Master\Certificate;
use App\Models\Transaction\ParticipantRegistrationCertificate;
use Illuminate\Http\Request;

class CertificateController extends BaseController
{
    public function index(Request $request, Certificate $trainingModel) {
        $data = $trainingModel->with("gambar", "gambar_logo", "type")->grid()->filters()->dtGet()->paginate($request->get("limit") ?? 10);

        $response = (new ListCertificateResource($data))->toArray($request);

        return $this->sendResponseSuccess($response);
    }

    public function show($slug, Certificate $trainingModel) {
        $training = $trainingModel->whereSlug($slug)->first();

        if(!$training) { return $this->sendResponseError([], "Resource Not Found", 404); }

        $response = (new DetailCertificateResource($training))->toArray(request());

        return $this->sendResponseSuccess($response);
    }

    public function registration(ParticipantRegistrationCertificateRequest $request) {
        $input = $request->validated();
        $model = new ParticipantRegistrationCertificate;

        $input["user_id"] = auth("api")->user()->id;
        $input["participant_code"] = time();
        $input["registration_date"] = date("Y-m-d", time());
        $input["id_card_file"] = $model->handleUploadIdCardFile($request);
        $input["last_diploma_file"] = $model->handleUploadLastDiplomaFile($request);
        $input["cert_of_employment_file"] = $model->handleUploadCertOfEmplymentFile($request);
        $input["cv_file"] = $model->handleUploadCVFile($request);
        $input["other_cert_file"] = $model->handleUploadOtherCertFile($request);

        return $model->handleStoreOrUpdate($input);
    }
}
