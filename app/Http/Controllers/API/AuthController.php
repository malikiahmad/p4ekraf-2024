<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\RegistrationRequest;
use App\Models\Auth\User;

class AuthController extends BaseController
{
    public function registration(RegistrationRequest $registrationRequest, User $userModel) {
        $input = $registrationRequest->validated();
        $input["status"] = "active";
        $input["password"] = bcrypt($input["password"]);

        $user = $userModel->create($input);

        $user = $user->assignRole("Peserta");

        return $this->sendResponseSuccess([], 'Registration was succeeded');
    }

    public function login(LoginRequest $loginRequest) {
        $input = $loginRequest->validated();

        if($token = auth("api")->attempt($input)) {
            $user = auth("api")->user();
            $response = [
                "email" => $user->email,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ];
            return $this->sendResponseSuccess($response, 'Login Success');
        }

        return $this->sendResponseError([], "Your credentials, is not correct. Please check your credential again.", 403);
    }
}
