<?php

namespace App\Http\Controllers;

use App\Models\Auth\Role;
use App\Models\Auth\User;
use App\Models\Globals\Notification;
use App\Models\Globals\TempFiles;
use App\Models\Master\Training;
use App\Models\Master\TrainingType;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function saveTempFiles(Request $request)
    {
        $this->beginTransaction();
        try {
            if ($file = $request->file('file')) {
                $file_path = str_replace('.'.$file->extension(), '', $file->hashName());
                $file_path .= '.'.$file->getClientOriginalExtension();

                $temp = new TempFiles;
                $temp->file_name = $file->getClientOriginalName();
                $temp->file_path = $file->storeAs('temp-files', $file_path, 'public');
                // $temp->file_type = $file->extension();
                $temp->file_size = $file->getSize();
                $temp->flag = $request->flag;
                $temp->save();
                return $this->commit([
                    'file' => TempFiles::find($temp->id)
                ]);
            }
            return $this->rollback(['message' => 'File not found']);
        }
        catch (\Exception $e) {
            return $this->rollback(['error' => $e->getMessage()]);
        }
    }

    public function deletTempFile($id, TempFiles $model) {
        $data = $model->find($id);dd($data);

        if(!$data) { return response()->json(400, ["message" => "File Not Found"]); }
        $data->delete();
        return response()->json(200, ["message" => "Success remove file"]);
    }

    public function testNotification($emails)
    {
        if ($rkia = Rkia::latest()->first()) {
            request()->merge([
                'module' => 'rkia_operation',
            ]);
            $emails = explode('--', trim($emails));
            $user_ids = User::whereIn('email', $emails)->pluck('id')->toArray();
            $rkia->addNotify([
                'message' => 'Waiting Approval RKIA '.$rkia->show_category.' '.$rkia->year,
                'url' => route('rkia.operation.summary', $rkia->id),
                'user_ids' => $user_ids,
            ]);
            $record = Notification::latest()->first();
            return $this->render('mails.notification', compact('record'));
        }
    }

    public function userNotification()
    {
        $notifications = auth()->user()->notifications()->latest()->simplePaginate(25);
        return $this->render('layouts.base.notification', compact('notifications'));
    }

    public function userNotificationRead(Notification $notification)
    {
        auth()->user()
            ->notifications()
            ->updateExistingPivot($notification, array('readed_at' => now()), false);
        return redirect($notification->full_url);
    }

    public function selectRole($search, Request $request)
    {
        $items = Role::keywordBy('name')->orderBy('name');
        switch ($search) {
            case 'all':
                $items = $items;
                break;
            case 'approver':
                $perms = str_replace('_', '.', $request->perms).'.approve';
                $items = $items->whereHas('permissions', function ($q) use ($perms) {
                    $q->where('name', $perms);
                });
                break;

            default:
                $items = $items->whereNull('id');
                break;
        }
        $items = $items->paginate();
        return $this->responseSelect2($items, 'name', 'id');
    }

    public function selectUser($search, Request $request)
    {
        $items = User::keywordBy('name')->whereHas('position')->orderBy('name');
        switch ($search) {
            case 'all':
                $items = $items;
                break;
        }
        $items = $items->paginate();

        $results = [];
        $more = $items->hasMorePages();
        foreach ($items as $item) {
            $results[] = ['id' => $item->id, 'text' => $item->name.' ('.($item->position->name ?? '').')'];
        }
        return response()->json(compact('results', 'more'));
    }

    public function selectTrainingType($search, Request $request) {
        $items = TrainingType::keywordBy('name')->orderBy('name');
        switch ($search) {
            case 'all':
                $items = $items;
                break;

            default:
                $items = $items->whereNull('id');
                break;
        }

        $items = $items->paginate();
        return $this->responseSelect2($items, 'name', 'id');
    }

    public function selectTraining($search, Request $request) {
        $items = Training::whereRaw("NOT EXISTS (SELECT training_id FROM certificates WHERE training_id = id)")->keywordBy('name')->orderBy('name');
        switch ($search) {
            case 'all':
                $items = $items;
                break;

            default:
                $items = $items->whereNull('id');
                break;
        }

        $items = $items->paginate();
        return $this->responseSelect2($items, 'name', 'id');
    }
}
