<?php

namespace App\Http\Controllers\Setting\User;

use App\Exports\Setting\UserTemplateExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\User\UserRequest;
use App\Models\Auth\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $module = 'setting_user';
    protected $routes = 'setting.user';
    protected $views = 'setting.user';
    protected $perms = 'setting_user';

    public function __construct()
    {
        $this->prepare([
            'module' => $this->module,
            'routes' => $this->routes,
            'views' => $this->views,
            'perms' => $this->perms,
            'permission' => $this->perms.'.view',
            'title' => 'Manajemen User',
            'breadcrumb' => [
                'Pengaturan Umum' => route($this->routes.'.index'),
                'Manajemen User' => route($this->routes.'.index'),
            ]
        ]);
    }

    public function index()
    {
        $this->prepare([
            'tableStruct' => [
                'datatable_1' => [
                    $this->makeColumn('name:num'),
                    $this->makeColumn('name:name|label:Nama|className:text-left'),
                    $this->makeColumn('name:username|label:Username|className:text-center'),
                    $this->makeColumn('name:email|label:Email|className:text-center'),
                    $this->makeColumn('name:role|label:Role|className:text-center'),
                    $this->makeColumn('name:status'),
                    $this->makeColumn('name:updated_by'),
                    $this->makeColumn('name:action'),
                ],
            ],
        ]);
        return $this->render($this->views.'.index');
    }

    public function grid()
    {
        $user = auth()->user();
        $records = User::grid()->filters()->dtGet();

        return \DataTables::of($records)
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->addColumn('role', function ($record) {
                if ($record->roles()->exists()) {
                    return implode('<br>', $record->roles()->pluck('name')->toArray());
                }
                return '-';
            })
            ->editColumn('status', function ($record) {
                return $record->labelStatus();
            })
            ->editColumn('updated_by', function ($record) {
                return $record->createdByRaw();
            })
            ->addColumn('action', function ($record) use ($user) {
                $actions = [];
                $actions[] = [
                    'type' => 'show',
                    'id' => $record->id
                ];
                $actions[] = [
                    'type' => 'edit',
                    'id' => $record->id,
                ];

                if ($user->id == 1) {
                    $actions[] = [
                        'label' => 'Reset Password',
                        'icon' => 'fa fa-retweet text-warning',
                        'class' => 'base-form--postByUrl',
                        'attrs' => 'data-swal-text="Reset password akan mengubah password menjadi: qwerty123456"',
                        'id' => $record->id,
                        'url' => route($this->routes.'.resetPassword', $record->id)
                    ];
                }

                if ($record->canDeleted()) {
                    $actions[] = [
                        'type' => 'delete',
                        'id' => $record->id,
                        'attrs' => 'data-confirm-text="'.__('Hapus').' User '.$record->name.'?"',
                    ];
                }
                return $this->makeButtonDropdown($actions);
            })
            ->rawColumns(['action','updated_by','status','position'])
            ->make(true);
    }

    public function create()
    {
        return $this->render($this->views.'.create');
    }

    public function store(UserRequest $request)
    {
        $record = new User;
        return $record->handleStoreOrUpdate($request);
    }

    public function show(User $record)
    {
        return $this->render($this->views.'.show', compact('record'));
    }

    public function edit(User $record)
    {
        return $this->render($this->views.'.edit', compact('record'));
    }

    public function update(UserRequest $request, User $record)
    {
        return $record->handleStoreOrUpdate($request);
    }

    public function destroy(User $record)
    {
        return $record->handleDestroy();
    }

    public function resetPassword(User $record)
    {
        return $record->handleResetPassword();
    }

    public function import()
    {
        if (request()->get('download') == 'template') {
            return $this->template();
        }
        return $this->render($this->views.'.import');
    }

    public function template()
    {
        $fileName = date('Y-m-d').' Template Import Data '. $this->prepared('title') .'.xlsx';
        return \Excel::download(new UserTemplateExport, $fileName);
    }

    public function importSave(Request $request)
    {
        $request->validate([
            'uploads.uploaded' => 'required'
        ],[],[
            'uploads.uploaded' => 'File'
        ]);

        $record = new User;
        return $record->handleImport($request);
    }
}
