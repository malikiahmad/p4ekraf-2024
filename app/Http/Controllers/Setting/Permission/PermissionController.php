<?php

namespace App\Http\Controllers\Setting\Permission;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\Permission\PermissionRequest;
use Illuminate\Http\Request;
use App\Models\Auth\Permission;

class PermissionController extends Controller
{
    protected $module = 'setting_permission';
    protected $routes = 'setting.permission';
    protected $views = 'setting.permission';
    protected $perms = 'setting_permission';

    public function __construct()
    {
        $this->prepare([
            'module' => $this->module,
            'routes' => $this->routes,
            'views' => $this->views,
            'perms' => $this->perms,
            'permission' => $this->perms.'.view',
            'title' => 'Permission',
            'breadcrumb' => [
                'Pengaturan Umum' => route($this->routes.'.index'),
                'Permission' => route($this->routes.'.index'),
            ]
        ]);
    }

    public function index()
    {
        $this->prepare([
            'tableStruct' => [
                'datatable_1' => [
                    $this->makeColumn('name:num'),
                    $this->makeColumn('name:name|label:Nama|className:text-left'),
                    $this->makeColumn('name:guard_name|label:Guard Name|className:text-center|width:200px'),
                    $this->makeColumn('name:updated_by'),
                    $this->makeColumn('name:action'),
                ],
            ],
        ]);
        return $this->render($this->views.'.index');
    }

    public function grid()
    {
        $records = Permission::grid()->filters()->dtGet();

        return \DataTables::of($records)
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('updated_by', function ($record) {
                return $record->createdByRaw();
            })
            ->addColumn('action', function ($record) {
                $actions = [];
                $actions[] = [
                    'type' => 'edit',
                    'id' => $record->id,
                ];

                if ($record->canDeleted()) {
                    $actions[] = [
                        'type' => 'delete',
                        'id' => $record->id,
                        'attrs' => 'data-confirm-text="'.__('Hapus').' Role '.$record->name.'?"',
                    ];
                }
                return $this->makeButtonDropdown($actions);
            })
            ->rawColumns(['action','updated_by'])
            ->make(true);
    }

    public function create()
    {
        return $this->render($this->views.'.create');
    }

    public function store(PermissionRequest $request)
    {
        $request->request->add(["guard_name" => "web"]);
        $record = new Permission;
        return $record->handleStoreOrUpdate($request);
    }

    public function edit(Permission $record)
    {
        return $this->render($this->views.'.edit', compact('record'));
    }

    public function update(PermissionRequest $request, Permission $record)
    {
        $request->request->add(["guard_name" => "web"]);
        return $record->handleStoreOrUpdate($request);
    }

    public function destroy(Permission $record)
    {
        return $record->handleDestroy();
    }

    public function import()
    {
        if (request()->get('download') == 'template') {
            return $this->template();
        }
        return $this->render($this->views.'.import');
    }

    // public function template()
    // {
    //     $fileName = date('Y-m-d').' Template Import Data '. $this->prepared('title') .'.xlsx';
    //     return \Excel::download(new RoleTemplateExport, $fileName);
    // }

    public function importSave(Request $request)
    {
        $request->validate([
            'uploads.uploaded' => 'required'
        ],[],[
            'uploads.uploaded' => 'File'
        ]);

        $record = new Permission;
        return $record->handleImport($request);
    }
}
