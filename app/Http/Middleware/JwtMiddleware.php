<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasHeader('Authorization')) {
            $authorization = $request->header('Authorization');
        } elseif ($request->hasHeader('authorization')) {
            $authorization = $request->header('authorization');
        } else {
            return response()->json([
                'success' => false,
                'message' => "Authorization Header not found",
            ], 401);
        }

        if(!auth("api")->user()) {
            return response()->json([
                'success' => false,
                'message' => "Unauthorized",
                'data' => [
                    'email' => 'Please login with your Email',
                    'password' => 'Please login with your correct Password',
                ],
            ], 401);
        }

        $token = null;
        $authorizationHeader = str_replace('bearer ', '', $authorization);
        $token = str_replace('Bearer ', '', $authorizationHeader);

        $request->request->add(['user_id' => auth("api")->user()->id]);
        return $next($request);
    }
}
