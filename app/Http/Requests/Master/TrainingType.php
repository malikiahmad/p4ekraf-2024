<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class TrainingType extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->record->id ?? 0;
        $rules = [
            "code"        => "required|string|max:30|unique:training_types,code," . $id,
            'name'        => 'required|string|max:100|unique:training_types,name,' . $id,
        ];

        return $rules;
    }
}
