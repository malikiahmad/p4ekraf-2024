<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class CertificateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->record->id ?? 0;

        $rules = [
            "training_type_id" => "required|exists:training_types,id",
            "code" => "required|min:6|max:30|unique:trainings,code," . $id,
            "name" => "required|min:20|max:255",
            "description" => "required|min:20|max:255",
            "place" => "max:255",
            "address" => "max:255",
            "link_wa_group" => "max:255",
            "syarat_ketentuan" => "max:255",
            "start_date_registration" => "required|date",
            "end_date_registration" => "required|date",
            "start_date" => "required|date",
            "end_date" => "required|date",
            "quota" => "required|numeric|min:1",
        ];

        return $rules;
    }
}
