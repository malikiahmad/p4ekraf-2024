<?php

namespace App\Http\Requests\API;

class RegistrationRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'username' => 'required|unique:sys_users,username,NULL',
            'email' => 'required|email|unique:sys_users,email,NULL',
            'password' => 'required|confirmed|min:8',
        ];
    }
}
