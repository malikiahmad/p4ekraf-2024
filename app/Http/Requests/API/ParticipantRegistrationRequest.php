<?php

namespace App\Http\Requests\API;

class ParticipantRegistrationRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "training_id" => "required|exists:trainings,id",
            "nik" => "required|string|min:16|max:16",
            "type_of_ktp" => "required|string|in:dki,non-dki",
            "birth_place" => "required|string|max:255",
            "birth_date" => "required|date",
            "gender" => "required|string|in:p,l",
            "residence_address" => "required|string|max:255",
            "identity_address" => "required|string|max:255",
            "phone_no" => "required|string|min:10|max:16",
            "work_experience" => "nullable|max:255",
            "current_job" => "nullable|max:255",
            "name_of_business_or_work_place" => "nullable|max:255",
            "last_education" => "nullable|max:75",
            "id_card_file" => "nullable|image|mimetypes:image/jpg,image/png,image/jpeg|max:2048",
            "last_diploma_file" => "nullable|image|mimetypes:image/jpg,image/png,image/jpeg|max:2048",
            "cert_of_employment_file" => "nullable|image|mimetypes:image/jpg,image/png,image/jpeg|max:2048",
            "cv_file" => "nullable|image|mimetypes:image/jpg,image/png,image/jpeg|max:2048",
            "other_cert_file" => "nullable|image|mimetypes:image/jpg,image/png,image/jpeg|max:2048",
        ];
    }
}
