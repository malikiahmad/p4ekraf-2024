<?php

namespace App\Http\Requests\Setting\Permission;

use App\Http\Requests\FormRequest;

class PermissionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->record->id ?? 0;
        return [
            'name' => 'required|string|max:255|unique:sys_permissions,name,'.$id,
        ];
    }
}
