<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ListCertificateResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $tempData = [];

        foreach($this as $key => $row) {
            $logoFile = $row->gambar_logo ? $row->gambar_logo->file_path : null;
            $imageFile = $row->gambar ? $row->gambar->file_path : null;
            $temp = [
                "id" => $row->id,
                "type" => $row->type->name,
                "code" => $row->code,
                "name" => $row->name,
                "slug" => $row->slug,
                "description" => $row->description,
                "syarat_ketentuan" => $row->syarat_ketentuan,
                "place" => $row->place,
                "address" => $row->address,
                "start_date_registration" => $row->start_date_registration,
                "date_date_registration" => $row->date_date_registration,
                "start_date" => $row->start_date,
                "date_date" => $row->date_date,
                "duration" => $row->duration,
                "quota" => $row->quota,
                "logo" => !is_null($row->gambar_logo) ? asset("public") . "/" . $logoFile : null,
                "image" => !is_null($row->gambar) ? asset("public") . "/" . $imageFile : null,
            ];

            array_push($tempData, $temp);
        }

        return [
            "data" => $tempData,
            "meta" => [
                "pagination" => [
                    "current_page" => $this->currentPage(),
                    "total" => $this->total(),
                    "per_page" => $this->perPage(),
                ],
            ],
        ];
    }
}
