<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrainingResourceShow extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => $this->type->name,
            "code" => $this->code,
            "name" => $this->name,
            "slug" => $this->slug,
            "description" => $this->description,
            "syarat_ketentuan" => $this->syarat_ketentuan,
            "place" => $this->place,
            "address" => $this->address,
            "start_date_registration" => $this->start_date_registration,
            "end_date_registration" => $this->end_date_registration,
            "start_date" => $this->start_date,
            "end_date" => $this->end_date,
            "duration" => $this->duration,
            "quota" => $this->quota,
            "logo" => $this->gambar_logo ? asset("public") . "/" . $this->gambar_logo->file_path : null,
            "image" => $this->gambar ? asset("public") . "/" . $this->gambar->file_path : null,
        ];
    }
}
