<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ListParticipantBySlugResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
   public function toArray($request)
    {
        $tempData = [];
        $urlAsset = asset("public");

        foreach($this as $key => $row) {

            $idCardFile = $row->id_card_file ? $urlAsset . "/" . $row->id_card_file : null;
            $lastDiplomaFile = $row->last_diploma_file ? $urlAsset . "/" . $row->last_diploma_file : null;
            $certOfEmploymentFile = $row->cert_of_employment_file ? $urlAsset . "/" . $row->cert_of_employment_file : null;
            $cvFile = $row->cv_file ? $urlAsset . "/" . $row->cv_file : null;
            $cvFile = $row->cv_file ? $urlAsset . "/" . $row->cv_file : null;
            $otherCertFile = $row->other_cert_file ? $urlAsset . "/" . $row->other_cert_file : null;

            $temp = [
                "id" => $row->id,
                "training_id" => $row->training_id,
                "participant_code" => $row->participant_code,
                "type_of_ktp" => $row->type_of_ktp,
                "name" => $row->name,
                "birth_place" => $row->birth_place,
                "birth_date" => $row->birth_date,
                "gender" => $row->gender,
                "residence_address" => $row->residence_address,
                "identity_address" => $row->identity_address,
                "phone_no" => $row->phone_no,
                "work_experience" => $row->work_experience,
                "current_job" => $row->current_job,
                "last_education" => $row->last_education,
                "id_card_file" => $idCardFile,
                "last_diploma_file" => $lastDiplomaFile,
                "cert_of_employment_file" => $certOfEmploymentFile,
                "cv_file" => $cvFile,
                "other_cert_file" => $otherCertFile,
                "user" => ["name" => $row->user->name, "username" => $row->user->username],
                "training" => ["name" => $row->training->name, "slug" => $row->training->slug],
            ];

            array_push($tempData, $temp);
        }

        return [
            "data" => $tempData,
            "meta" => [
                "pagination" => [
                    "current_page" => $this->currentPage(),
                    "total" => $this->total(),
                    "per_page" => $this->perPage(),
                ],
            ],
        ];
    }
}
