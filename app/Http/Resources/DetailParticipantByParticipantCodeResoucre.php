<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailParticipantByParticipantCodeResoucre extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $urlAsset = asset("public");
        $idCardFile = $this->id_card_file ? $urlAsset . "/" . $this->id_card_file : null;
        $lastDiplomaFile = $this->last_diploma_file ? $urlAsset . "/" . $this->last_diploma_file : null;
        $certOfEmploymentFile = $this->cert_of_employment_file ? $urlAsset . "/" . $this->cert_of_employment_file : null;
        $cvFile = $this->cv_file ? $urlAsset . "/" . $this->cv_file : null;
        $cvFile = $this->cv_file ? $urlAsset . "/" . $this->cv_file : null;
        $otherCertFile = $this->other_cert_file ? $urlAsset . "/" . $this->other_cert_file : null;

        $temp = [
            "id" => $this->id,
            "training_id" => $this->training_id,
            "participant_code" => $this->participant_code,
            "type_of_ktp" => $this->type_of_ktp,
            "name" => $this->name,
            "birth_place" => $this->birth_place,
            "birth_date" => $this->birth_date,
            "gender" => $this->gender,
            "residence_address" => $this->residence_address,
            "identity_address" => $this->identity_address,
            "phone_no" => $this->phone_no,
            "work_experience" => $this->work_experience,
            "current_job" => $this->current_job,
            "last_education" => $this->last_education,
            "id_card_file" => $idCardFile,
            "last_diploma_file" => $lastDiplomaFile,
            "cert_of_employment_file" => $certOfEmploymentFile,
            "cv_file" => $cvFile,
            "other_cert_file" => $otherCertFile,
            "user" => ["name" => $this->user->name, "username" => $this->user->username],
            "training" => ["name" => $this->training->name, "slug" => $this->training->slug],
        ];
        return $temp;
    }
}
