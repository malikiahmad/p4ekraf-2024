<?php

namespace App\Models\Master;

use App\Models\Master\TrainingType;
use App\Models\Model;
use Illuminate\Support\Str;
use App\Models\Globals\TempFiles;
use Carbon\Carbon;

class Certificate extends Model
{
    protected $fillable = [
        "id", "training_type_id", "code", "name", "slug", "description", "place", "address", "link_wa_group", "syarat_ketentuan", "start_date_registration", "end_date_registration", "start_date", "end_date", "duration", "quota",
        "logo", "image", "created_at", "created_by", "updated_at", "updated_by",
    ];

    protected $casts = [
        "quota" => "integer",
    ];

    public function type() {
        return $this->belongsTo(TrainingType::class, "training_type_id");
    }

    public function gambar_logo() {
        return $this->belongsTo(TempFiles::class, "logo");
    }

    public function gambar() {
        return $this->belongsTo(TempFiles::class, "logo");
    }

    /** SCOPE **/
    public function scopeGrid($query)
    {
        return $query->latest();
    }

    public function scopeFilters($query)
    {
        if(request()->get("training_type_id")) { $query = $query->whereIn("training_type_id", request()->get("training_type_id")); }

        return $query->filterBy(['code','name']); // filter like
    }

    /** SAVE DATA */
    public function handleStoreOrUpdate($request)
    {
        $this->beginTransaction();
        try {
            $input = $request->all();

            if(!empty($input["logo"]) && is_array($input["logo"]) && array_key_exists("temp_files_ids", $input["logo"])) {
                $input["logo"] = $input["logo"]["temp_files_ids"][0];
            }

            if(!empty($input["image"]) && is_array($input["image"]) && array_key_exists("temp_files_ids", $input["image"])) {
                $input["image"] = $input["image"]["temp_files_ids"][0];
            }

            // $start = strtotime($input["start_date"]);
            // $end = strtotime($input["end_date"]);
            // $selisih_hari = ceil(($end - $start) / (60 * 60 * 24));
            // $input["duration"] = abs((int) $selisih_hari);
            // Tanggal awal dan akhir
            $tanggalAwal = Carbon::parse($input["start_date"]);
            $tanggalAkhir = Carbon::parse($input["end_date"]);
            $durasiHariKerja = 0;
            while ($tanggalAwal->lessThanOrEqualTo($tanggalAkhir)) {
                if ($tanggalAwal->dayOfWeek !== 6 && $tanggalAwal->dayOfWeek !== 0) {
                    $durasiHariKerja++;
                }
                $tanggalAwal->addDay();
            }
            $input["duration"] = $durasiHariKerja;
            $input["slug"] = Str::slug($input["name"]);

            $this->fill($input);

            $this->save();
            $this->saveLogNotify();

            return $this->commitSaved();
        } catch (\Exception $e) {
            return $this->rollbackSaved($e);
        }
    }

    /** DELETE DATA */
    public function handleDestroy()
    {
        $this->beginTransaction();
        try {
            if (!$this->canDeleted()) {
                return $this->rollback(__('base.error.related'));
            }
            $this->saveLogNotify();
            $this->delete();

            return $this->commitDeleted();
        } catch (\Exception $e) {
            return $this->rollbackDeleted($e);
        }
    }

    public function saveLogNotify()
    {
        $data = $this->name;
        $routes = request()->get('routes');
        switch (request()->route()->getName()) {
            case $routes.'.store':
                $this->addLog('Membuat Data '.$data);
                break;
            case $routes.'.update':
                $this->addLog('Mengubah Data '.$data);
                break;
            case $routes.'.destroy':
                $this->addLog('Menghapus Data '.$data);
                break;
        }
    }

    /** OTHER FUNCTION **/
    public function canDeleted()
    {
        return auth()->user()->hasPermissionTo("master_certificate.delete");
    }

    public function checkPerms($permission)
    {
        return $this->hasPermissionTo($permission);
    }
}
