<?php

namespace App\Models\Transaction;

use App\Models\Auth\User;
use App\Models\Master\Certificate;
use App\Models\Model;
use App\Support\Constants;

class ParticipantRegistrationCertificate extends Model
{
    protected $table = "participant_registration_certificates";
    protected $fillable = [
        "id", "user_id", "certificate_id", "participant_code",
        "registration_date", "status", "nik", "type_of_ktp",
        "birth_place", "birth_date", "gender", "residence_address",
        "identity_address", "phone_no", "work_experience", "current_job",
        "name_of_business_or_work_place", "last_education", "id_card_file",
        "last_diploma_file", "cert_of_employment_file", "cv_file", "other_cert_file",
        "created_at", "created_by", "updated_at", "updated_by",
    ];

    public function user() {
        return $this->belongsTo(User::class, "user_id");
    }

    public function certificate() {
        return $this->belongsTo(Certificate::class, "certificate_id");
    }

    public function scopeGrid($query)
    {
        return $query->latest();
    }

    public function scopeFilters($query)
    {
        if(request()->get("user_fullname")) {
            $query = $query->whereHas("user", function($query) {
                $query->where("name", "REGEXP", request()->get("user_fullname"));
            });
        }

        if(request()->get("certificate")) {
            $query = $query->whereHas("certificate", function($query) {
                $query->where("name", "REGEXP", request()->get("certificate"));
            });
        }

        if(request()->get("user_id")) { $query = $query->whereIn("user_id", request()->get("user_id")); }

        return $query->filterBy(['participant_code','registration_date', "status", "nik"]); // filter like
    }

    /** SAVE DATA */
    public function handleStoreOrUpdate($request)
    {
        $this->beginTransaction();
        try {
            if(is_array($request)) {
                $input = $request;
            } else {
                $input = $request->all();
            }

            $this->fill($input);

            $this->save();
            $this->saveLogNotify();

            return $this->commitSaved();
        } catch (\Exception $e) {
            return $this->rollbackSaved($e);
        }
    }

    /** DELETE DATA */
    public function handleDestroy()
    {
        $this->beginTransaction();
        try {
            if (!$this->canDeleted()) {
                return $this->rollback(__('base.error.related'));
            }
            $this->saveLogNotify();
            $this->delete();

            return $this->commitDeleted();
        } catch (\Exception $e) {
            return $this->rollbackDeleted($e);
        }
    }

    public function saveLogNotify()
    {
        $data = $this->name;
        $routes = request()->get('routes');
        switch (request()->route()->getName()) {
            case $routes.'.store':
                $this->addLog('Membuat Data '.$data);
                break;
            case $routes.'.update':
                $this->addLog('Mengubah Data '.$data);
                break;
            case $routes.'.destroy':
                $this->addLog('Menghapus Data '.$data);
                break;
        }
    }

    /** OTHER FUNCTION **/
    public function canDeleted()
    {
        return auth()->user()->hasPermissionTo("participant_certificate.delete");
    }

    public function checkPerms($permission)
    {
        return $this->hasPermissionTo($permission);
    }

    /**
     * Others Function
    */
    public function handleUploadIdCardFile($request) {
        $pathFile = null;
        if ($file = $request->file('id_card_file')) {
            $file_path = str_replace('.'.$file->extension(), '', $file->hashName());
            $file_path .= '.'.$file->getClientOriginalExtension();

            $path = Constants::PATH_FILE_PARTICIPANT_REGISTRATION_CERTIFICATE . "/id_card";

            $pathFile = $file->storeAs($path, $file_path, 'public');
        }
        return $pathFile;
    }

    public function handleUploadCVFile($request) {
        $pathFile = null;
        if ($file = $request->file('cv_file')) {
            $file_path = str_replace('.'.$file->extension(), '', $file->hashName());
            $file_path .= '.'.$file->getClientOriginalExtension();

            $path = Constants::PATH_FILE_PARTICIPANT_REGISTRATION_CERTIFICATE . "/cv_file";

            $pathFile = $file->storeAs($path, $file_path, 'public');
        }
        return $pathFile;
    }

    public function handleUploadOtherCertFile($request) {
        $pathFile = null;
        if ($file = $request->file('other_cert_file')) {
            $file_path = str_replace('.'.$file->extension(), '', $file->hashName());
            $file_path .= '.'.$file->getClientOriginalExtension();

            $path = Constants::PATH_FILE_PARTICIPANT_REGISTRATION_CERTIFICATE . "/other_cert_file";

            $pathFile = $file->storeAs($path, $file_path, 'public');
        }
        return $pathFile;
    }

    public function handleUploadCertOfEmplymentFile($request) {
        $pathFile = null;
        if ($file = $request->file('cert_of_employment_file')) {
            $file_path = str_replace('.'.$file->extension(), '', $file->hashName());
            $file_path .= '.'.$file->getClientOriginalExtension();

            $path = Constants::PATH_FILE_PARTICIPANT_REGISTRATION_CERTIFICATE . "/cert_of_employment_file";

            $pathFile = $file->storeAs($path, $file_path, 'public');
        }
        return $pathFile;
    }

    public function handleUploadLastDiplomaFile($request) {
        $pathFile = null;
        if ($file = $request->file('last_diploma_file')) {
            $file_path = str_replace('.'.$file->extension(), '', $file->hashName());
            $file_path .= '.'.$file->getClientOriginalExtension();

            $path = Constants::PATH_FILE_PARTICIPANT_REGISTRATION_CERTIFICATE . "/last_diploma_file";

            $pathFile = $file->storeAs($path, $file_path, 'public');
        }
        return $pathFile;
    }
}
