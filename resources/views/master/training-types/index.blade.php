@extends('layouts.lists')

@section('filters')
	<div class="row">
		<div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="code" placeholder="{{ __('Kode') }}">
		</div>
		<div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="name" placeholder="{{ __('Nama') }}">
		</div>
	</div>
@endsection

@section('buttons')
	@if (auth()->user()->checkPerms($perms.'.create'))
		@include('layouts.forms.btnAdd')
	@endif
@endsection
