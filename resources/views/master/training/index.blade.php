@extends('layouts.lists')

@section('filters')
	<div class="row">
		<div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="code" placeholder="{{ __('Kode') }}">
		</div>
		<div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="name" placeholder="{{ __('Nama') }}">
		</div>
        <div class="col-12 col-sm-6 col-xl-2 pb-2">
            <select name="training_type_id[]" class="form-control filter-control js-example-basic-multiple base-plugin--select2-ajax"
                data-url="{{ route('ajax.selectTrainingType', 'all') }}"
                placeholder="{{ __('Pilih Tipe') }}" data-post="training_type_id[]" multiple>
                <option value="">{{ __('Pilih Tipe') }}</option>
            </select>
        </div>
	</div>
@endsection

@section('buttons')
	@if (auth()->user()->checkPerms($perms.'.create'))
		@include('layouts.forms.btnAdd')
	@endif
@endsection
