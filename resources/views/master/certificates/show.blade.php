@extends('layouts.pageSubmit')

@section('action', route($routes.'.update', $record->id))
@section('req-method', "PATCH")
@section('autocomplete', 'on')

@section('page-content')
    @csrf
	@method('PATCH')
    <!-- layouts form -->
    <div class="row mb-3">
        <div class="col-sm-12">
            <div class="card card-custom">
                @section('card-header')
                    <div class="card-header">
                        <h3 class="card-title">{{ "Data " . $title }}</h3>
                        <div class="card-toolbar">
                            @section('card-toolbar')
                                @include('layouts.forms.btnBackTop')
                            @show
                        </div>
                    </div>
                @show

                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tipe Pelatihan') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <select name="training_type_id" class="form-control base-plugin--select2-ajax"
                                        data-url="{{ route('ajax.selectTrainingType', 'all') }}"
                                        placeholder="{{ __('Pilih Salah Satu') }}" disabled>
                                        <option value="">{{ __('Pilih Salah Satu') }}</option>
                                        @if ($record->type)
                                            <option value="{{ $record->type->id }}" selected>{{ $record->type->name }}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Kode Pelatihan') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="code" class="form-control" placeholder="{{ __('Kode Pelatihan') }}" value="{{ $record->code }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Nama Pelatihan') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="name" class="form-control" placeholder="{{ __('Nama Pelatihan') }}" value="{{ $record->name }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Deskripsi Pelatihan') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="description" class="form-control" id="description" rows="10" placeholder="{{ __('Deskripsi Pelatihan') }}" disabled>{{ $record->description }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Long,Lat') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="place" class="form-control" placeholder="{{ __('Long,Lat') }}" value="{{ $record->place }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Alamat') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="address" class="form-control" id="address" rows="10" placeholder="{{ __('Alamat') }}" disabled>{{ $record->address }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Syarat Ketentuan') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="syarat_ketentuan" class="form-control" id="address" rows="10" placeholder="{{ __('Syarat Ketentuan') }}" disabled>{{ $record->syarat_ketentuan }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Link WA Group') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="link_wa_group" class="form-control" placeholder="{{ __('Link WA Group') }}" value="{{ $record->link_wa_group }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Mulai Registrasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 col-md-4 parent-group">
                                    <input type="date" name="start_date_registration" class="form-control" placeholder="{{ __('Tanggal Mulai Registrasi') }}" value="{{ $record->start_date_registration }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Berakhir Registrasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 col-md-4 parent-group">
                                    <input type="date" name="end_date_registration" class="form-control" placeholder="{{ __('Tanggal Berakhir Registrasi') }}" value="{{ $record->start_date_registration }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Mulai') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 col-md-4 parent-group">
                                    <input type="date" name="start_date" class="form-control" placeholder="{{ __('Tanggal Mulai') }}" value="{{ $record->start_date }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Berakhir') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 col-md-4 parent-group">
                                    <input type="date" name="end_date" class="form-control" placeholder="{{ __('Tanggal Berakhir') }}" value="{{ $record->start_date }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Kuota') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="number" name="quota" class="form-control" placeholder="{{ __('Kuota') }}"
                                        value="{{ $record->quota }}" disabled
                                        oninput="this.value = !!this.value && Math.abs(this.value) >= 0 ? Math.abs(this.value) : null"
                                    >
                                </div>
                            </div>

                            <!-- Logo -->
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Logo') }}</label>
                                <div class="col-sm-8 parent-group trainig-logo-attachment-input">
                                    <div class="custom-file">
                                        @if($record->gambar_logo)
                                            <div class="progress-container w-100" data-uid="{{ uniqid() }}">
                                                <div class="alert alert-custom alert-light fade show py-2 px-4 mb-0 mt-2 success-uploaded" role="alert">
                                                    <div class="alert-icon">
                                                        <i class="text-warning far fa-file-image"></i>
                                                    </div>
                                                    <div class="alert-text text-left">
                                                        <input type="hidden" name="logo" value="{{ $record->gambar_logo->id }}">
                                                        <div>Upload File:</div>
                                                        <a href="{{ asset('public') }}/{{ $record->gambar_logo->file_path }}" target="_blank" class="text-primary">{{ $record->gambar_logo->file_name }}</a>
                                                    </div>
                                                    <div class="alert-close">
                                                        @if(!in_array($page_action, ['show', 'approval']))
                                                        <button type="button" class="close base-form--remove-files" data-toggle="
                                                            tooltip" title="" data-original-title="Remove" data-id="{{ $record->logo }}"
                                                            data-name="{{ $record->gambar_logo->file_name }}"
                                                        >
                                                            <span aria-hidden="true">
                                                                <i class="ki ki-close"></i>
                                                            </span>
                                                        </button>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="progress uploading">
                                                    <div class="progress-bar bar-{{ uniqid() }}progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Done</div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <!-- Image -->
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Gambar') }}</label>
                                <div class="col-sm-8 parent-group trainig-image-attachment-input">
                                    <div class="custom-file">
                                        @if($record->gambar)
                                            <div class="progress-container w-100" data-uid="{{ uniqid() }}">
                                                <div class="alert alert-custom alert-light fade show py-2 px-4 mb-0 mt-2 success-uploaded" role="alert">
                                                    <div class="alert-icon">
                                                        <i class="text-warning far fa-file-image"></i>
                                                    </div>
                                                    <div class="alert-text text-left">
                                                        <input type="hidden" name="image" value="{{ $record->gambar->id }}">
                                                        <div>Upload File:</div>
                                                        <a href="{{ asset('public') }}/{{ $record->gambar->file_path }}" target="_blank" class="text-primary">{{ $record->gambar->file_name }}</a>
                                                    </div>
                                                    <div class="alert-close">
                                                        @if(!in_array($page_action, ['show', 'approval']))
                                                        <button type="button" class="close base-form--remove-files" data-toggle="
                                                            tooltip" title="" data-original-title="Remove" data-id="{{ $record->image }}"
                                                            data-name="{{ $record->gambar->file_name }}"
                                                        >
                                                            <span aria-hidden="true">
                                                                <i class="ki ki-close"></i>
                                                            </span>
                                                        </button>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="progress uploading">
                                                    <div class="progress-bar bar-{{ uniqid() }}progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Done</div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- End of Card Body -->
                <div class="card-footer">
                    <div class="d-flex justify-content-end">
                        @include('layouts.forms.btnBack')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of layouts form -->
@endsection
