@extends('layouts.pageSubmit')

@section('action', route($routes.'.store'))
@section('req-method', "POST")
@section('autocomplete', 'on')

@section('page-content')
    @csrf
	@method('POST')
    <!-- layouts form -->
    <div class="row mb-3">
        <div class="col-sm-12">
            <div class="card card-custom">
                @section('card-header')
                    <div class="card-header">
                        <h3 class="card-title">{{ $title . " Baru" }}</h3>
                        <div class="card-toolbar">
                            @section('card-toolbar')
                                @include('layouts.forms.btnBackTop')
                            @show
                        </div>
                    </div>
                @show

                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tipe Sertifikasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <select name="training_type_id" class="form-control base-plugin--select2-ajax"
                                        data-url="{{ route('ajax.selectTrainingType', 'all') }}"
                                        placeholder="{{ __('Pilih Salah Satu') }}">
                                        <option value="">{{ __('Pilih Salah Satu') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Kode Sertifikasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="code" class="form-control" placeholder="{{ __('Kode Sertifikasi') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Nama Sertifikasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="name" class="form-control" placeholder="{{ __('Nama Sertifikasi') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Deskripsi Sertifikasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="description" class="form-control" id="description" rows="10" placeholder="{{ __('Deskripsi Sertifikasi') }}"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Long,Lat') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="place" class="form-control" placeholder="{{ __('Long,Lat') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Alamat') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="address" class="form-control" id="address" rows="10" placeholder="{{ __('Alamat') }}"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Syarat Ketentuan') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="syarat_ketentuan" class="form-control" id="address" rows="10" placeholder="{{ __('Syarat Ketentuan') }}"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Link WA Group') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="link_wa_group" class="form-control" placeholder="{{ __('Link WA Group') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Mulai Registrasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 col-md-4 parent-group">
                                    <input type="date" name="start_date_registration" class="form-control" placeholder="{{ __('Tanggal Mulai Registrasi') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Berakhir Registrasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 col-md-4 parent-group">
                                    <input type="date" name="end_date_registration" class="form-control" placeholder="{{ __('Tanggal Berakhir Registrasi') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Mulai') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 col-md-4 parent-group">
                                    <input type="date" name="start_date" class="form-control" placeholder="{{ __('Tanggal Mulai') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Berakhir') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 col-md-4 parent-group">
                                    <input type="date" name="end_date" class="form-control" placeholder="{{ __('Tanggal Berakhir') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Kuota') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="number" name="quota" class="form-control" placeholder="{{ __('Kuota') }}"
                                        oninput="this.value = !!this.value && Math.abs(this.value) >= 0 ? Math.abs(this.value) : null"
                                    >
                                </div>
                            </div>
                            <!-- Logo -->
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Logo') }}</label>
                                <div class="col-sm-8 parent-group trainig-logo-attachment-input">
                                    <div class="custom-file">
                                        <input type="file"
                                            name="logo"
                                            class="custom-file-input base-form--save-temp-files"
                                            data-name="logo"
                                            data-parent-class="trainig-logo-attachment-input"
                                            data-max-size="50000"
                                            data-max-file="1"
                                            accept="image/jpg,image/jpeg,image/gif,image/png,image/bmp">
                                        <label class="custom-file-label" for="file">Choose File</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Image -->
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Gambar') }}</label>
                                <div class="col-sm-8 parent-group trainig-image-attachment-input">
                                    <div class="custom-file">
                                        <input type="file"
                                            name="image"
                                            class="custom-file-input base-form--save-temp-files"
                                            data-name="image"
                                            data-parent-class="trainig-image-attachment-input"
                                            data-max-size="50000"
                                            data-max-file="1"
                                            accept="image/jpg,image/jpeg,image/gif,image/png,image/bmp">
                                        <label class="custom-file-label" for="file">Choose File</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- End of Card Body -->
                <div class="card-footer">
                    <div class="d-flex justify-content-end">
                        @include('layouts.forms.btnBack')
                        @include('layouts.forms.btnSubmitPage')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of layouts form -->
@endsection

@push("scripts")
    <script src="{{ asset('assets/js/general_script.js') }}"></script>
@endpush
