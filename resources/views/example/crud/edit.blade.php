@extends('layouts.form', ['container' => 'container-fluid'])

@section('action', route($routes.'.update', $record->id))

@section('card-body')
	@method('PATCH')
	@include($views.'.includes.notes')
	<br>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group row">
				<label class="col-md-4 col-form-label">{{ __('Tahun') }}</label>
				<div class="col-md-8 parent-group">
					<input type="text" name="year"
						class="form-control base-plugin--datepicker-3"
						value="{{ $record->year }}"
						data-options='@json([
							"startDate" => (string) $record->year,
							"endStart" => ""
						])'
						placeholder="{{ __('Tahun') }}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-4 col-form-label">{{ __('Tanggal') }}</label>
				<div class="col-md-8 parent-group">
					<input type="text" name="date"
						class="form-control base-plugin--datepicker"
						value="{{ $record->show_date }}"
						data-options='@json([
							"startDate" => (string) $record->show_date,
							"endStart" => ""
						])'
						placeholder="{{ __('Tanggal') }}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-4 col-form-label">{{ __('Rentang Tanggal') }}</label>
				<div class="col-md-8 parent-group">
					<div class="input-group">
						<input type="text" name="range_start"
							class="form-control base-plugin--datepicker range_start"
							value="{{ $record->show_range_start }}"
							data-options='@json([
								"orientation" => "bottom",
								"startDate" => (string) $record->show_range_start,
								"endStart" => ""
							])'
							placeholder="{{ __('Mulai') }}">
						<div class="input-group-append">
							<span class="input-group-text">
								<i class="la la-ellipsis-h"></i>
							</span>
						</div>
						<input type="text" name="range_end"
							class="form-control base-plugin--datepicker range_end"
							value="{{ $record->show_range_end }}"
							data-options='@json([
								"orientation" => "bottom",
								"startDate" => (string) $record->show_range_start,
								"endStart" => ""
							])'
							placeholder="{{ __('Selesai') }}">
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-4 col-form-label">{{ __('Lampiran') }}</label>
				<div class="col-md-8 parent-group">
					@php
						$files = $record->files($module)->where('flag', 'attachments')->get();
					@endphp
					<div class="custom-file">
					    <input type="hidden"
					    	name="attachments[uploaded]"
					    	class="uploaded"
					    	value="{{ $files->count() ? 1 : null }}">
					    <input type="file" multiple
					    	class="custom-file-input base-form--save-temp-files"
					    	data-name="attachments"
					    	data-container="parent-group"
					    	data-max-size="20024"
					    	data-max-file="100"
					    	@if($files->count() >= 100) disabled @endif
					    	accept="*">
					    <label class="custom-file-label">@if($files->count()) Add @else Choose @endif File</label>
					</div>
					@foreach ($files as $file)
					    <div class="progress-container w-100" data-uid="{{ $file->id }}">
					        <div class="alert alert-custom alert-light fade show py-2 px-4 mb-0 mt-2 success-uploaded" role="alert">
					            <div class="alert-icon">
					                <i class="{{ $file->file_icon }}"></i>
					            </div>
					            <div class="alert-text text-left">
					                <input type="hidden"
					                	name="attachments[files_ids][]"
					                	value="{{ $file->id }}">
					                <div>Uploaded File:</div>
					                <a href="{{ $file->file_url }}" target="_blank" class="text-primary">
					                	{{ $file->file_name }}
					                </a>
					            </div>
					            <div class="alert-close">
					                <button type="button"
					                	class="close base-form--remove-temp-files"
					                	data-toggle="tooltip"
					                	data-original-title="Remove">
					                    <span aria-hidden="true">
					                        <i class="ki ki-close"></i>
					                    </span>
					                </button>
					            </div>
					        </div>
					    </div>
					@endforeach
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group row">
				<label class="col-md-4 col-form-label">{{ __('Input') }}</label>
				<div class="col-md-8 parent-group">
					<input type="text" name="input"
						class="form-control"
						value="{{ $record->input }}"
						placeholder="{{ __('Input') }}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-4 col-form-label">{{ __('Option') }}</label>
				<div class="col-md-8 parent-group">
					<select name="option"
						class="form-control base-plugin--select2"
						placeholder="{{ __('Pilih Salah Satu') }}">
						<option value="">{{ __('Pilih Salah Satu') }}</option>
						@foreach ($options as $key => $text)
							<option value="{{ $key }}" @if($key == $record->option) selected @endif>{{ $text }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-4 col-form-label">{{ __('Textarea') }}</label>
				<div class="col-md-8 parent-group">
					<textarea name="textarea"
						class="form-control"
						placeholder="{{ __('Textarea') }}">{!! $record->textarea !!}</textarea>
				</div>
			</div>

		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-detail">
					<thead>
						<tr>
							<th class="text-center width-60px">No</th>
							<th class="text-center">Example</th>
							<th class="text-center">Jabatan</th>
							<th class="text-center">User</th>
							<th class="text-center">Deskripsi</th>
							<th class="text-center width-60px valign-middle">
								<button type="button"
									class="btn btn-sm btn-icon btn-circle btn-primary btn-add">
									<i class="fa fa-plus"></i>
								</button>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($record->details as $detail)
							<tr data-key="{{ $loop->iteration }}">
								<td class="text-center no">{{ $loop->iteration }}</td>
								<td class="text-left parent-group" style="width: 250px; max-width: 250px;">
									{{-- Manual Select2 --}}
									<select name="details[{{ $loop->iteration }}][example_id]"
										class="form-control base-plugin--select2"
										placeholder="{{ __('Pilih Salah Satu') }}">
										<option value="">{{ __('Pilih Salah Satu') }}</option>
										@foreach ($examples as $val)
											<option value="{{ $val->id }}" @if($val->id == $detail->example_id) selected @endif>{{ $val->name }}</option>
										@endforeach
									</select>
								</td>
								<td class="text-left parent-group" style="width: 250px; max-width: 250px;">
									{{-- Ajax Select2 --}}
									<select name="details[{{ $loop->iteration }}][position_id]"
										class="form-control base-plugin--select2-ajax position_id"
										data-url="{{ route('ajax.selectPosition', ['search' => 'all']) }}"
										placeholder="{{ __('Pilih Salah Satu') }}">
										<option value="">{{ __('Pilih Salah Satu') }}</option>
										@if ($detail->user && ($position = $detail->user->position))
											<option value="{{ $position->id }}" selected>{{ $position->name }}</option>
										@endif
									</select>
								</td>
								<td class="text-left parent-group" style="width: 250px; max-width: 250px;">
									{{-- Ajax Select2 Combobox --}}
									<select name="details[{{ $loop->iteration }}][user_id]"
										class="form-control base-plugin--select2-ajax user_id"
										data-url="{{ route('ajax.selectUser', [
											'search' => 'by_position',
											'position_id' => '',
										]) }}"
										data-url-origin="{{ route('ajax.selectUser', [
											'search' => 'by_position'
										]) }}"
										@if (!$detail->user) disabled  @endif
										placeholder="{{ __('Pilih Salah Satu') }}">
										<option value="">{{ __('Pilih Salah Satu') }}</option>
										@if ($user = $detail->user)
											<option value="{{ $user->id }}" selected>{{ $user->name }} ({{ $user->position->name }})</option>
										@endif
									</select>
								</td>
								<td class="text-left parent-group">
									<textarea name="details[{{ $loop->iteration }}][description]"
										class="form-control"
										placeholder="{{ __('Textarea') }}">{!! $detail->description !!}</textarea>
								</td>
								<td class="text-center valign-middle">
									<button type="button"
										@if($loop->count == 1) disabled @endif
										class="btn btn-sm btn-icon btn-circle btn-danger btn-remove">
										<i class="fa fa-trash"></i>
									</button>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('card-footer')
	<div class="d-flex justify-content-between">
		@include('layouts.forms.btnBack')
		@include('layouts.forms.btnDropdownSubmit')
	</div>
@endsection

@push('scripts')
	@include($views.'.includes.scripts')
@endpush
