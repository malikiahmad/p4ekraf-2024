@extends('layouts.pageSubmit')

@section('action', route($routes.'.update', $record->id))
@section('req-method', "PATCH")
@section('autocomplete', 'on')

@section('page-content')
    @csrf
	@method('PATCH')
    <!-- layouts form -->
    <div class="row mb-3">
        <div class="col-sm-12">
            <div class="card card-custom">
                @section('card-header')
                    <div class="card-header">
                        <h3 class="card-title">{{ "Data " . $title . " " . $record->participant_code }}</h3>
                        <div class="card-toolbar">
                            @section('card-toolbar')
                                @include('layouts.forms.btnBackTop')
                            @show
                        </div>
                    </div>
                @show

                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tipe Sertifikasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="name" class="form-control" placeholder="{{ __('Tipe Sertifikasi') }}" value="{{ $record->certificate->type->name }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Nama Sertifikasi') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="name" class="form-control" placeholder="{{ __('Nama Sertifikasi') }}" value="{{ $record->certificate->name }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Nama Peserta') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="code" class="form-control" placeholder="{{ __('Nama Peserta') }}" value="{{ $record->user->name }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Id Peserta') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="code" class="form-control" placeholder="{{ __('Id Peserta') }}" value="{{ $record->participant_code }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Pendaftaran') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="place" class="form-control" placeholder="{{ __('Tanggal Pendaftaran') }}" value="{{ date("d/m/Y", strtotime($record->registration_date)) }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Status') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="place" class="form-control" placeholder="{{ __('Status') }}" value="{{ strtoupper($record->status) }}" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('NIK') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="place" class="form-control" placeholder="{{ __('NIK') }}"
                                        value="{{ $record->nik }}" disabled
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Jenis KTP') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="place" class="form-control" placeholder="{{ __('Jenis KTP') }}"
                                        value="{{ strtoupper($record->type_of_ktp) }}" disabled
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tempat Lahir') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="place" class="form-control" placeholder="{{ __('Tempat Lahir') }}"
                                        value="{{ strtoupper($record->birth_place) }}" disabled
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Tanggal Lahir') }}</label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="place" class="form-control" placeholder="{{ __('Tanggal Lahir') }}"
                                        value="{{ date("d/m/Y", strtotime($record->birth_date)) }}" disabled
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Alamat Domisili') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="address" class="form-control" id="address" rows="10" placeholder="{{ __('Alamat Domisili') }}" disabled>{{ $record->residence_address }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Alamat KTP') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="address" class="form-control" id="address" rows="10" placeholder="{{ __('Alamat KTP') }}" disabled>{{ $record->identity_address }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Nomor Telepon') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="quota" class="form-control" placeholder="{{ __('Nomor Telepon') }}"
                                        value="{{ $record->phone_no }}" disabled
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Pengalaman Kerja') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <textarea name="address" class="form-control" id="address" rows="10" placeholder="{{ __('Pengalaman Kerja') }}" disabled>{{ $record->work_experience }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Pekerjaan Saat Ini') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="quota" class="form-control" placeholder="{{ __('Pekerjaan Saat Ini') }}"
                                        value="{{ $record->current_job }}" disabled
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Nama Tempat Kerja/Usaha') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="quota" class="form-control" placeholder="{{ __('Nama Tempat Kerja/Usaha') }}"
                                        value="{{ $record->name_of_business_or_work_place }}" disabled
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Pendidikan Terakhir') }}<b class="text-danger">*</b></label>
                                <div class="col-sm-8 parent-group">
                                    <input type="text" name="quota" class="form-control" placeholder="{{ __('Pendidikan Terakhir') }}"
                                        value="{{ strtoupper($record->last_education) }}" disabled
                                    >
                                </div>
                            </div>

                            <!-- Logo -->
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('KTP Attachment') }}</label>
                                <div class="col-sm-8 parent-group trainig-logo-attachment-input">
                                    <div class="custom-file">
                                        @if($record->id_card_file)
                                            <div class="progress-container w-100" data-uid="{{ uniqid() }}">
                                                <div class="alert alert-custom alert-light fade show py-2 px-4 mb-0 mt-2 success-uploaded" role="alert">
                                                    <div class="alert-icon">
                                                        <i class="text-warning far fa-file-image"></i>
                                                    </div>
                                                    <div class="alert-text text-left">
                                                        <input type="hidden" name="logo" value="{{ $record->id_card_file }}">
                                                        <div>Upload File:</div>
                                                        <a href="{{ asset('public') }}/{{ $record->id_card_file }}" target="_blank" class="text-primary">KTP</a>
                                                    </div>
                                                </div>

                                                <div class="progress uploading">
                                                    <div class="progress-bar bar-{{ uniqid() }}progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Done</div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Ijazah Terakhir') }}</label>
                                <div class="col-sm-8 parent-group trainig-logo-attachment-input">
                                    <div class="custom-file">
                                        @if($record->last_diploma_file)
                                            <div class="progress-container w-100" data-uid="{{ uniqid() }}">
                                                <div class="alert alert-custom alert-light fade show py-2 px-4 mb-0 mt-2 success-uploaded" role="alert">
                                                    <div class="alert-icon">
                                                        <i class="text-warning far fa-file-image"></i>
                                                    </div>
                                                    <div class="alert-text text-left">
                                                        <input type="hidden" name="logo" value="{{ $record->last_diploma_file }}">
                                                        <div>Upload File:</div>
                                                        <a href="{{ asset('public') }}/{{ $record->last_diploma_file }}" target="_blank" class="text-primary">Ijazah</a>
                                                    </div>
                                                </div>

                                                <div class="progress uploading">
                                                    <div class="progress-bar bar-{{ uniqid() }}progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Done</div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Sertifikat') }}</label>
                                <div class="col-sm-8 parent-group trainig-logo-attachment-input">
                                    <div class="custom-file">
                                        @if($record->cert_of_employment_file)
                                            <div class="progress-container w-100" data-uid="{{ uniqid() }}">
                                                <div class="alert alert-custom alert-light fade show py-2 px-4 mb-0 mt-2 success-uploaded" role="alert">
                                                    <div class="alert-icon">
                                                        <i class="text-warning far fa-file-image"></i>
                                                    </div>
                                                    <div class="alert-text text-left">
                                                        <input type="hidden" name="logo" value="{{ $record->cert_of_employment_file }}">
                                                        <div>Upload File:</div>
                                                        <a href="{{ asset('public') }}/{{ $record->cert_of_employment_file }}" target="_blank" class="text-primary">Sertifikat</a>
                                                    </div>
                                                </div>

                                                <div class="progress uploading">
                                                    <div class="progress-bar bar-{{ uniqid() }}progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Done</div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('Curriculum Vitae') }}</label>
                                <div class="col-sm-8 parent-group trainig-logo-attachment-input">
                                    <div class="custom-file">
                                        @if($record->cv_file)
                                            <div class="progress-container w-100" data-uid="{{ uniqid() }}">
                                                <div class="alert alert-custom alert-light fade show py-2 px-4 mb-0 mt-2 success-uploaded" role="alert">
                                                    <div class="alert-icon">
                                                        <i class="text-warning far fa-file-image"></i>
                                                    </div>
                                                    <div class="alert-text text-left">
                                                        <input type="hidden" name="logo" value="{{ $record->cv_file }}">
                                                        <div>Upload File:</div>
                                                        <a href="{{ asset('public') }}/{{ $record->cv_file }}" target="_blank" class="text-primary">CV</a>
                                                    </div>
                                                </div>

                                                <div class="progress uploading">
                                                    <div class="progress-bar bar-{{ uniqid() }}progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Done</div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">{{ __('File Pendukung') }}</label>
                                <div class="col-sm-8 parent-group trainig-logo-attachment-input">
                                    <div class="custom-file">
                                        @if($record->other_cert_file)
                                            <div class="progress-container w-100" data-uid="{{ uniqid() }}">
                                                <div class="alert alert-custom alert-light fade show py-2 px-4 mb-0 mt-2 success-uploaded" role="alert">
                                                    <div class="alert-icon">
                                                        <i class="text-warning far fa-file-image"></i>
                                                    </div>
                                                    <div class="alert-text text-left">
                                                        <input type="hidden" name="logo" value="{{ $record->other_cert_file }}">
                                                        <div>Upload File:</div>
                                                        <a href="{{ asset('public') }}/{{ $record->other_cert_file }}" target="_blank" class="text-primary">File Pendukung</a>
                                                    </div>
                                                </div>

                                                <div class="progress uploading">
                                                    <div class="progress-bar bar-{{ uniqid() }}progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">Done</div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- End of Card Body -->
                <div class="card-footer">
                    <div class="d-flex justify-content-end">
                        @include('layouts.forms.btnBack')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of layouts form -->
@endsection
