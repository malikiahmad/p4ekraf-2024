@extends('layouts.lists')

@section('filters')
	<div class="row">
		<div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="user_fullname" placeholder="{{ __('Nama Peserta') }}">
		</div>
        <div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="certificate" placeholder="{{ __('Sertifikasi') }}">
		</div>
		<div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="participant_code" placeholder="{{ __('Id Peserta') }}">
		</div>
        <div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="status" placeholder="{{ __('Status') }}">
		</div>
        <div class="col-12 col-sm-6 col-xl-2 pb-2">
			<input type="text" class="form-control filter-control" data-post="nik" placeholder="{{ __('NIK') }}">
		</div>
	</div>
@endsection

@section('buttons')
	@if (auth()->user()->checkPerms($perms.'.create'))
		{{-- @include('layouts.forms.btnAdd') --}}
	@endif
@endsection
