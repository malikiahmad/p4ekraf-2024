const SGUPengajuanPembayaranModule = function () {
	let moduleName = 'sgu_pengajuan_pembayaran';
	let moduleScope = '[data-module-name="sgu_pengajuan_pembayaran"]';
	
	return {
		init: function () {
			if (ModuleApp.getName() === moduleName) {
				console.log("Pengajuan Pembayaran SGU Module")
			}
		},
		updateStatusPengajuanPembayaran: function(el, status) {
			const recordId = $(el).data("record-id");
			const formData = new FormData();
			formData.append("status", status);

			$.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
				url: `${window.location.origin}/sgu/pengajuan-pembayaran/${recordId}/update-pengajuan-status`,
				method: 'POST',
				dataType: 'JSON',
				cache: false,
				processData: false,
				contentType: false,
				data: formData,
				success:function(resp)
				{                    
					$.gritter.add({
						title: resp.title != undefined ? resp.title : 'Success!',
						text: resp.message != undefined ? resp.message : 'Data saved successfull!',
						image: BaseUtil.getUrl('assets/media/ui/check.png'),
						sticky: false,
						time: '3000'
					});

					setTimeout(()=>{
						window.location.href = `${window.location.origin}/sgu/pengajuan-pembayaran`;
					})
				},
				error: function(resp) {
					$.gritter.add({
						title: resp.title != undefined ? resp.title : 'Failed!',
						text: resp.message != undefined ? resp.message : 'Data failed to save!',
						image: BaseUtil.getUrl('assets/media/ui/cross.png'),
						sticky: false,
						time: 3000
					});
				}
			});
		},
		submitPengajuanPembayaranSGU: function(){
			const formEl = $(moduleScope + " form");

            let formData = new FormData(formEl[0]);
			formData.append("is_edit", 1)
			BaseForm.appendUploadedFilesToFormData(formData,formEl);
            BaseForm.submitCustomFormData(formData,formEl, {
				callbackSuccess: function(resp, form, options){
					setTimeout(()=>{
						window.location.href = "/sgu/pengajuan-pembayaran";
					},1000)
				},
			});
		},
		documentEvent: function () {

			$(document).on('click', moduleScope + " .reject-sgu-payment-request", function(e) {
				e.preventDefault();
				SGUPengajuanPembayaranModule.updateStatusPengajuanPembayaran(this, 0);
            });

			$(document).on('click', moduleScope + " .approve-sgu-payment-request", function(e) {
				e.preventDefault();
				SGUPengajuanPembayaranModule.updateStatusPengajuanPembayaran(this, 3);
            });

			$(document).on('click', moduleScope + ' .submit-data', function(e) {
				e.preventDefault();

				const status = $(this).data("status");
				$(moduleScope + " form input[name='status']").val(status);

				Swal.fire({
					title: BaseApp.lang('confirm.save.title'),
					text: BaseApp.lang('confirm.save.text'),
					icon: 'warning',
					showCancelButton: true,
					confirmButtonText: BaseApp.lang('confirm.save.ok'),
					cancelButtonText: BaseApp.lang('confirm.save.cancel'),
				}).then(function (result) {
					if (result.isConfirmed) {
						SGUPengajuanPembayaranModule.submitPengajuanPembayaranSGU();
					}
				});
			})
		},
	}
}();

// webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = SGUPengajuanPembayaranModule;
}