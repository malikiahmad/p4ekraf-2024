const SGUPengajuanModule = function () {
	let moduleName = 'sgu_pengajuan';
	let moduleScope = '[data-module-name="sgu_pengajuan"]';
	
	return {
		init: function () {
			if (ModuleApp.getName() === moduleName) {
				console.log("Pengajuan SGU Module")
			}
		},
		calcualateRentTimePriod: function () {
			const rentStartDate = $(moduleScope + " input#rent_start_date").val();
			const rentEndDate = $(moduleScope + " input#rent_end_date").val();

			if(rentEndDate && rentStartDate){
				const rentTimePeriod = this.getRangeDate(rentStartDate, rentEndDate);
				$(moduleScope + " input#rent_time_period").val(rentTimePeriod == 0? "-" :  rentTimePeriod + " Hari");
			}
		},
		getRangeDate: function(startDate, endDate) {
			const startDateMoment = moment(startDate, "YYYY-MM-DD");
			const endDateMoment = moment(endDate, "YYYY-MM-DD");

			let d = startDateMoment;
			let range = 0;
			while (+d.toDate() < +endDateMoment.toDate()) {
				d = d.add(1, 'days');
				range++;
			}
			return range;
		},
		updateStatusPengajuan: function(el, status) {
			let element = $(el);
			const recordId = element.data("id");
			
			const formData = new FormData();
			formData.append("status", status);

			$.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
				url: `${window.location.origin}/sgu/pengajuan/${recordId}/update-pengajuan-status`,
				method: 'POST',
				dataType: 'JSON',
				cache: false,
				processData: false,
				contentType: false,
				data: formData,
				success:function(resp)
				{                    
					$.gritter.add({
						title: resp.title != undefined ? resp.title : 'Success!',
						text: resp.message != undefined ? resp.message : 'Data saved successfull!',
						image: BaseUtil.getUrl('assets/media/ui/check.png'),
						sticky: false,
						time: '3000'
					});

					setTimeout(()=>{
						window.location.href = `${window.location.origin}/sgu/pengajuan`;
					})
				},
				error: function(resp) {
					$.gritter.add({
						title: resp.title != undefined ? resp.title : 'Failed!',
						text: resp.message != undefined ? resp.message : 'Data failed to save!',
						image: BaseUtil.getUrl('assets/media/ui/cross.png'),
						sticky: false,
						time: 3000
					});
				}
			});
		},
		submitPengajuanSGU: function(){
			const formEl = $(moduleScope + " form");
            let formData = new FormData(formEl[0]);
			BaseForm.appendUploadedFilesToFormData(formData,formEl);
            BaseForm.submitCustomFormData(formData,formEl, {
				callbackSuccess: function(resp, form, options){
					setTimeout(()=>{
						window.location.href = "/sgu/pengajuan";
					},1000)
				}
			});
		},
		documentEvent: function () {
			$(document).on('change', moduleScope + " input#rent_start_date", function(e) {
				SGUPengajuanModule.calcualateRentTimePriod();
            });

			$(document).on('click', moduleScope + " .reject-sgu-submition", function(e) {
				SGUPengajuanModule.updateStatusPengajuan(this, 0);
            });

			$(document).on('click', moduleScope + " .approve-sgu-submition", function(e) {
				SGUPengajuanModule.updateStatusPengajuan(this, 3);
            });

			$(document).on('change', moduleScope + " input#rent_end_date", function(e){
				SGUPengajuanModule.calcualateRentTimePriod();
            });

			$(document).on('click', moduleScope + ' .submit-data', function(e) {
				e.preventDefault();

				const status = $(this).data("status");
				$(moduleScope + " form input[name='status']").val(status);

				Swal.fire({
					title: BaseApp.lang('confirm.save.title'),
					text: BaseApp.lang('confirm.save.text'),
					icon: 'warning',
					showCancelButton: true,
					confirmButtonText: BaseApp.lang('confirm.save.ok'),
					cancelButtonText: BaseApp.lang('confirm.save.cancel'),
				}).then(function (result) {
					if (result.isConfirmed) {
						SGUPengajuanModule.submitPengajuanSGU();
					}
				});
			})
		},
	}
}();

// webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = SGUPengajuanModule;
}